<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>eState - Text Output</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/style-v2.css?ver=4" rel="stylesheet" type="text/css" media="all">
<link href="css/style-print-v2.css?ver=4" rel="stylesheet" type="text/css" media="print">
<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
<script>
	$(function() {
		
		
	  $(".showcomments").click(function( event ) {
           event.preventDefault();
           $(".note").toggle();
		  
	});
});
	</script>
</head>

<body>

<header>
<div class="container">
<p class="author">e-State Planner by User Name</p>
</div>
</header>

<footer>
   <div class="container"><p class="date">Date: 20 February, 2017 - 09:00</p>
	   <a href="#" class="showcomments">Hide/Show  Comments For Developers</a><img src="img/logo-2.png" alt="Hull E-State Planner&trade;" class="logo"/> 
		</div>
		</footer>

<div class="container">
<section class="front-page">
	<div class="note">Notes have been added in red, use the button in the footer to toggle them on/off.</div>
	<p class="author">e-State Planner by User Name</p>
	<h1>Will Plan</h1>
	<h2>Client Name &amp; Spouse Name</h2>
	
	</section>
 <section>
    <h1>Table of Contents</h1>
	 <ul class="toc">
		 <li><a href="#section01">Client Information</a></li>
		 <li><a href="#section02">Family Data</a></li>
		 <li><a href="#section03">Assets</a></li>
		 <li><a href="#section04">Depts &amp; Liabilities</a></li>
		 <li><a href="#section05">Appointment</a></li>
		 <li><a href="#section06">Surviving Spouse / No Spouse / No Decendants</a></li>
	 </ul>
	</section>
  <section>
    <h1 id="section01">Client Information</h1>
    <div class="fieldset full table">
      <ul>
        <li><strong>First  Name:</strong> <span>xxxxxxxxxx</span></li>
        <li><strong>Middle  Name:</strong> <span>xxxxxxxxx</span></li>
        <li><strong>Last Name: </strong> <span>xxxxxxxxxxx</span></li>
        <li><strong>Also Known  As:</strong> <span>xxxxxxx</span></li>
        <li><strong>Email:</strong> <span>xxxxxxxxxxx</span></li>
        <li><strong>Gender:</strong> <span>Male/Female</span></li>
        <li><strong>DOB:</strong> <span>xxxxxxxxxx</span></li>
        <li><strong>Citizenship:</strong> <span>xxxxxx</span></li>
        <li><strong>Residency:</strong> <span>xxxxxx</span></li>
        <li><strong>Address:</strong> <span>xxxxxx</span></li>
        <li class="parent"><strong>Marital Status:</strong> <span>xxxxx</span>
          <ul> 
           <li><strong>Name of Spouse:</strong> <span>xxxxxx</span> <div class="note">Displayed if married</div></li>
            <li><strong>Name of Ex-Spouse: </strong><span>xxxxx </span> <div class="note">Displayed if divorced</div></li>
          </ul>
        </li>
        <li class="parent"><strong>Prior Relationship:</strong> <span class="hidden">True/False</span>
           <ul>
            <li><strong>Name: </strong><span>xxxxxx</span></li>
            <li><strong>Date: </strong><span>xxxxxx</span></li>
            <li><strong>Contract: </strong><span>xxxxxx</span></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="clearfix"></div>
  </section>
  <section>
    <h1 id="section02">Family Data</h1>
    <div class="child">
      <h2>Children </h2>
      <div class="fieldset col2 table hidden">
        <ul>
          <li><strong>Number of  children:</strong> <span>xxxxxx</span></li>
          <li><strong>Future  Children:</strong> <span>xxxxxx</span></li>
        </ul>
      </div>
      <div class="clearfix"></div>
      <div class="child">
      <h3>Child 1</h3>
       <div class="row">
       <div class="col-xs-6">
        
        <div class="fieldset full table">
          <ul>
            <li><strong>Name:</strong> <span>xxxxxx</span></li>
            <li><strong>DOB:</strong> <span>xxxxxx</span></li>
            <li><strong>Age:</strong> <span>xxxxxx</span></li>
            <li><strong>Citizenship:</strong> <span>xxxxxx</span></li>
            <li><strong>Residency:</strong> <span>xxxxxx</span></li>
            <li><strong>Status:</strong> <span>xxxxxx</span></li>
			 <li class="parent"><strong>Marital  Status:</strong> <span>xxxxx</span>
<ul>
                <li><strong>Name of  Spouse:</strong> <span>xxxxxx</span> <div class="note">Displayed if married</div></li>
                <li><strong>Name of Ex-Spouse:</strong> <span>xxxxx</span> <div class="note">Displayed if divorced</div></li>
              </ul>
            </li>
           
             <li class="parent"><strong>Special  Circumstances</strong>
  <ul>
                <li class="parent"><strong>Disabled:</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div>
<ul>
                    <li><strong>Details:</strong> <span>xxxxxx</span></li>
                    <li><strong>Benefits:</strong> <span><span>xxxxxx</span></span></li>
                  </ul>
                </li>
                <li class="parent"><strong>Relationship  Difficulties:</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div>
<ul>
                    <li><strong>Details:</strong> <span>xxxxxx</span></li>
                  </ul>
                </li>
                <li class="parent"><strong>Financial  Management Issues:</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div>
<ul>
                    <li><strong>Details:</strong> <span>xxxxxx</span></li>
                  </ul>
                </li>
                <li class="parent"><strong>Other  Special Circumstances:</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div>
<ul>
                    <li><strong>Details:</strong> <span>xxxxxx</span></li>
                  </ul>
                </li>
              </ul>
            </li>
			  <li class="hidden"><strong>Children:</strong> <span>Yes/No</span></li>
              <li class="hidden"><strong>Current Number  of Children:</strong> <span>xxxxxx</span></li>
              <li class="hidden"><strong>Future  Children: </strong><span>Yes/No</span></li>
          </ul>
        </div>
       
		</div>
         <div class="col-xs-6">
         <div class="child">
            <h4>Grand Children (Child 1's Children)</h4>
          
            <div class="fieldset full table">
              <ul>
                <li class="title"><strong>Child 1</strong>
                  <ul>
                    <li><strong>Name:</strong> <span>xxxxxx</span></li>
                    <li><strong>Approximate  Age:</strong> <span>xxxxxx</span></li>
                    <li><strong>Citizenship: </strong><span>xxxxxx</span></li>
                    <li><strong>Residency:</strong> <span>xxxxxx</span></li>
                    <li class="parent"><strong>Marital Status:</strong> <span>xxxxx</span>
<ul>
                        <li><strong>Name of Spouse:</strong> <span>xxxxxx</span> <div class="note">Displayed if married</div></li>
                        <li><strong>Name of Ex-Spouse:</strong> <span>xxxxx</span> <div class="note">Displayed if divorced</div></li>
                      </ul>
                    </li>
                    <li class="parent"><strong>Special Circumstances</strong>         
  <ul>
                        <li class="parent"><strong>Disabled:</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div>
<ul>
                            <li><strong>Details:</strong> <span>xxxxxx</span></li>
                            <li><strong>Benefits: </strong><span>xxxxxx</span></li>
                          </ul>
                        </li>
                        <li class="parent"><strong>Relationship Difficulties:</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div>
<ul>
                            <li><strong>Details:</strong> <span>xxxxxx</span></li>
                          </ul>
                        </li>
                        <li class="parent"><strong>Financial Management Issues:</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div>
<ul>
                            <li><strong>Details:</strong> <span>xxxxxx</span></li>
                          </ul>
                        </li>
                        <li class="parent"><strong>Other Special Circumstances:</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div>
<ul>
                            <li><strong>Details:</strong>  <span>xxxxxx</span></li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    
                  </ul>
					<div class="clearfix"></div>
                </li>
                <li class="title"><strong>Child 2 </strong>
                  <ul>
                    <li><strong>Name:</strong> <span>xxxxxx</span></li>
                    <li><strong>Approximate  Age:</strong> <span>xxxxxx</span></li>
                    <li><strong>Citizenship: </strong><span>xxxxxx</span></li>
                    <li><strong>Residency:</strong> <span>xxxxxx</span></li>
                    <li class="parent"><strong>Marital Status:</strong> <span>xxxxx</span>
<ul>
                        <li><strong>Name of Spouse:</strong> <span>xxxxxx</span> <div class="note">Displayed if married</div></li>
                        <li><strong>Name of Ex-Spouse:</strong> <span>xxxxx</span> <div class="note">Displayed if divorced</div></li>
                      </ul>
                    </li>
                    <li class="parent"><strong>Special Circumstances</strong>
  <ul>
                        <li><strong>None</strong></li>
                      </ul>
                    </li>
                    
                  </ul>
					<div class="clearfix"></div>
                </li>
              </ul>
            </div>
          
          <div class="clearfix"></div>
        </div>
		</div>
	  </div>
          
      </div>
    </div>
    <div class="child">
      <h2>Other family members</h2>
      <div class="fieldset col2 table">
        <ul>
          <li class="parent"><strong>Mother:</strong> <span class="hidden">Yes/No</span><ul>
              <li><strong>Name:</strong> <span>xxxxx</span></li>
			  <li><strong>Status:</strong> <span>xxxxx</span> <div class="note">Display "Deceased" if not alive</div></li>
            </ul>
          </li>
          <li class="parent"><strong>Father:</strong> <span class="hidden">Yes/No</span><ul>
              <li><strong>Name:</strong> <span>xxxxx</span></li>
              <li><strong>Status:</strong> <span>xxxxx</span> <div class="note">Display "Deceased" if not alive</div></li>
            </ul>
          </li>
          <li><strong>Step  Parents:</strong> <span>Yes/No</span></li>
          <li class="parent hidden"><strong>Siblings:</strong> <span class="hidden">Yes/No</span><ul>
              <li class="hidden"><strong>Number of  Siblings:</strong> <span>xxxxx</span></li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="child">
      <h3>Sibling 1</h3> <div class="note">Displayed if siblings present</div>
       <div class="row">
       <div class="col-xs-6">
        
        <div class="fieldset full table">
          <ul>
             <li><strong>Name:</strong> <span>xxxxx</span></li>
                <li class="parent"><strong>Marital  Status:</strong> <span>xxxxx</span>
<ul>
                    <li><strong>Name of  Spouse:</strong> <span>xxxxx</span> <div class="note">Displayed if married</div></li>
                    <li><strong>Name of  Ex-Spouse:</strong> <span>xxxxx</span> <div class="note">Displayed if divorced</div></li>
                  </ul>
                </li>
               <li class="parent"><strong>Siblings’  Parent(s)</strong>
                  <ul>
                    <li><strong>Same as  Client’s:</strong> <span>Yes/No</span></li>
                    <li><strong>Name of  Parents:</strong> <span>xxxxx</span></li>
                  </ul>
                </li>
                <li class="parent hidden"><strong>Sibling’s  Children (your Niece(s)/Nephew(s))</strong>
                  <ul>
                    <li><strong>Children:</strong> <span>Yes/No</span></li>
                    <li><strong>Current  Number of Children:</strong> <span>xxxxx</span></li>
                    <li><strong>Future  Children:</strong> <span>Yes/No</span></li>
                  </ul>
                </li>
			</ul>
        </div>
       
		</div>
         <div class="col-xs-6">
         <div class="child">
            <h4>Children (your Niece(s)/Nephew(s))</h4>
          
            <div class="fieldset full table">
              <ul>
                <li class="title parent"><strong>Child 1</strong>
                  <ul>
                    <li><strong>Name:</strong> <span>xxxxxx</span></li>
                    <li><strong>Approximate  Age:</strong> <span>xxxxxx</span></li>
                    <li><strong>Citizenship: </strong><span>xxxxxx</span></li>
                    <li><strong>Residency:</strong> <span>xxxxxx</span></li>
                    <li class="parent"><strong>Marital Status:</strong> <span>xxxxx</span>
<ul>
                        <li><strong>Name of Spouse:</strong> <span>xxxxxx</span> <div class="note">Displayed if married</div></li>
                        <li><strong>Name of Ex-Spouse:</strong> <span>xxxxx</span> <div class="note">Displayed if divorced</div></li>
                      </ul>
                    </li>
                    <li class="parent"><strong>Special Circumstances</strong>
  <ul>
                        <li><strong>None</strong></li>
                      </ul>
                    </li>
                    
                  </ul>
					<div class="clearfix"></div>
                </li>
                <li class="title parent"><strong>Child 2 </strong>
                  <ul>
                    <li><strong>Name:</strong> <span>xxxxxx</span></li>
                    <li><strong>Approximate  Age:</strong> <span>xxxxxx</span></li>
                    <li><strong>Citizenship: </strong><span>xxxxxx</span></li>
                    <li><strong>Residency:</strong> <span>xxxxxx</span></li>
                    <li class="parent"><strong>Marital Status:</strong> <span>xxxxx</span>
<ul>
                        <li><strong>Name of Spouse:</strong> <span>xxxxxx</span> <div class="note">Displayed if married</div></li>
                        <li class="hidden"><strong>Name of Ex-Spouse:</strong> <span>xxxxx</span> <div class="note">Displayed if divorced</div></li>
                      </ul>
                    </li>
                    <li class="parent"><strong>Special Circumstances</strong>
                       <ul>
                        <li><strong>None</strong></li>
                      </ul>
                    </li>
                    
                  </ul>
					<div class="clearfix"></div>
                </li>
              </ul>
            </div>
          
          <div class="clearfix"></div>
        </div>
		</div>
	  </div>
          
      </div>
    </div>
    <div class="child">
      <h2>Spouse&rsquo;s family members</h2>
      <div class="fieldset col2 table">
        <ul>
          <li class="parent"><strong>Mother:</strong> <span class="hidden">Yes/No</span><ul>
              <li><strong>Name:</strong> <span>xxxxx</span></li>
              <li><strong>Status:</strong> <span class="hidden">Yes/No</span> <div class="note">Display "Deceased" if not alive</div></li>
            </ul>
          </li>
          <li class="parent"><strong>Father: </strong><span class="hidden">Yes/No</span><ul>
              <li><strong>Name:</strong> <span>xxxxx</span></li>
              <li><strong>Status:</strong> <span class="hidden">Yes/No</span> <div class="note">Display "Deceased" if not alive</div></li>
            </ul>
          </li>
          <li><strong>Step  Parents:</strong> <span>Yes/No</span></li>
          <li class="parent hidden"><strong>Siblings:</strong> <span>Yes/No</span>
<ul>
              <li><strong>Number of  Siblings:</strong> <span>xxxxx</span></li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="child">
      <h3>Sibling 1</h3>
       <div class="row">
       <div class="col-xs-6">
        
        <div class="fieldset full table">
          <ul>
             <li><strong>Name:</strong> <span>xxxxx</span></li>
                <li class="parent"><strong>Marital  Status:</strong> <span>xxxxx</span>
<ul>
                    <li><strong>Name of  Spouse:</strong> <span>xxxxx</span> <div class="note">Displayed if married</div></li>
                    <li class="hidden"><strong>Name of  Ex-Spouse:</strong> <span>xxxx</span> <div class="note">Displayed if divorced</div></li>
                  </ul>
                </li>
               <li class="parent"><strong>Siblings’  Parent(s)</strong>
                 <ul>
                    <li><strong>Same as  Client’s:</strong> <span>Yes/No</span></li>
					 <li><strong>Name of  Parents:</strong> <span>xxxxx</span> <div class="note">Hidden if previous is set to yes</div></li>
                  </ul>
                </li>
                <li class="parent hidden"><strong>Sibling’s  Children (your Niece(s)/Nephew(s)) </strong>
                  <ul>
                    <li><strong>Children:</strong> <span>Yes/No</span></li>
                    <li><strong>Current  Number of Children:</strong> <span>xxxx</span></li>
                    <li><strong>Future  Children:</strong> <span>Yes/No</span></li>
                  </ul>
                </li>
			</ul>
        </div>
       
		</div>
         <div class="col-xs-6">
         <div class="child">
            <h4>Children (your Niece(s)/Nephew(s))</h4>
          
            <div class="fieldset full table">
              <ul>
                <li class="title"><strong>Child 1</strong>
                  <ul>
                    <li><strong>Name:</strong> <span>xxxxxx</span></li>
                    <li><strong>Approximate  Age:</strong> <span>xxxxxx</span></li>
                    <li><strong>Citizenship: </strong><span>xxxxxx</span></li>
                    <li><strong>Residency:</strong> <span>xxxxxx</span></li>
                    <li class="parent"><strong>Marital Status:</strong> <span>xxxxx</span>
<ul>
                        <li><strong>Name of Spouse:</strong> <span>xxxxxx</span> <div class="note">Displayed if married</div></li>
                        <li><strong>Name of Ex-Spouse:</strong> <span>xxxx</span> <div class="note">Displayed if divorced</div></li>
                      </ul>
                    </li>
                    <li class="parent"><strong>Special Circumstances</strong>                   
  <ul>
                        <li><strong>None</strong></li>
                      </ul>
                    </li>
                    
                  </ul>
					<div class="clearfix"></div>
                </li>
                <li class="title"><strong>Child 2 </strong>
                  <ul>
                    <li><strong>Name:</strong> <span>xxxxxx</span></li>
                    <li><strong>Approximate  Age:</strong> <span>xxxxxx</span></li>
                    <li><strong>Citizenship: </strong><span>xxxxxx</span></li>
                    <li><strong>Residency:</strong> <span>xxxxxx</span></li>
                    <li class="parent"><strong>Marital Status:</strong> <span>xxxxx</span>
<ul>
                        <li><strong>Name of Spouse:</strong> <span>xxxxxx</span> <div class="note">Displayed if married</div></li>
                        <li><strong>Name of Ex-Spouse:</strong> <span>xxxx</span> <div class="note">Displayed if divorced</div></li>
                      </ul>
                    </li>
                    <li class="parent"><strong>Special Circumstances</strong>                    
  <ul>
                        <li><strong>None</strong></li>
                      </ul>
                    </li>
                    
                  </ul>
                  <div class="clearfix"></div>
                </li>
              </ul>
            </div>
          
          <div class="clearfix"></div>
        </div>
		</div>
	  </div>
          
      </div>
    </div>
  </section>
  <section>
    <h1 id="section03">Assets</h1>
    <div class="fieldset full  table">
      <ul>
        <li  class="title parent"><strong>Asset 1 </strong>
          <ul>
            <li><strong>Type:</strong> <span>xxxx</span></li>
            <li><strong>Details:</strong> <span>xxxx</span></li>
            <li class="parent"><strong>Ownership</strong>
            <ul>
              <li><strong>Type:</strong> <span>xxxx</span></li>
              <li><strong>Owner  Name:</strong> <span>xxxx</span></li>
            </ul></li>
            <li><strong>Gross  Value:</strong> <span>xxxx</span></li>
            <li><strong>Associated  Debts:</strong> <span>xxxx</span></li>
            <li><strong>Income  Tax:</strong> <span>xxxx</span></li>
            <li><strong>Probate  Tax:</strong> <span>xxxx</span></li>
            <li class="parent"><strong>Notes</strong> <div class="note">Repeated element</div>
            <ul>
              <li><strong>Note  Title:</strong> <span>xxxx</span></li>
              <li><strong>Note Date:</strong> <span>xxxx</span></li>
              <li><strong>Note  Details:</strong> <span>xxxx</span></li>
            </ul></li>
          </ul>
			<div class="clearfix"></div>
        </li>
      </ul>
      <ul>
        <li  class="title parent" ><strong>Asset 2 </strong>
          <ul>
            <li><strong>Type:</strong> <span>xxxx</span></li>
            <li><strong>Details:</strong> <span>xxxx</span></li>
            <li class="parent"><strong>Ownership</strong>
            <ul>
              <li><strong>Type:</strong> <span>xxxx</span></li>
              <li><strong>Owner  Name:</strong> <span>xxxx</span></li>
            </ul></li>
            <li><strong>Gross  Value:</strong> <span>xxxx</span></li>
            <li><strong>Associated  Debts:</strong> <span>xxxx</span></li>
            <li><strong>Income  Tax:</strong> <span>xxxx</span></li>
            <li><strong>Probate  Tax:</strong> <span>xxxx</span></li>
            <li class="parent"><strong>Notes</strong> <div class="note">Repeated element</div>
            <ul>
              <li><strong>Note  Title:</strong> <span>xxxx</span></li>
              <li><strong>Note Date:</strong> <span>xxxx</span></li>
              <li><strong>Note  Details:</strong> <span>xxxx</span></li>
            </ul></li>
          </ul>
			<div class="clearfix"></div>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
  </section>
  <section>
    <h1 id="section04">Debts &amp; Liabilities</h1>
    <div class="fieldset full child-col2 table">
      <ul>
        <li  class="title parent"><strong>Item 1</strong>
          <ul>
            <li><strong>Type:</strong> <span>xxxx</span></li>
            <li><strong>Details:</strong> <span>xxxx</span></li>
            <li><strong>Amount:</strong> <span>xxxx</span></li>
            <li class="parent"><strong>Notes</strong> <div class="note">Repeated element</div>
            <ul>
              <li><strong>Note  Title:</strong> <span>xxxx</span></li>
              <li><strong>Note Date:</strong> <span>xxxx</span></li>
              <li><strong>Note  Details:</strong> <span>xxxx</span></li>
            </ul></li>
          </ul>
			<div class="clearfix"></div>
        </li>
      </ul>
      <ul>
        <li  class="title parent"><strong>Item 2</strong>
          <ul>
            <li><strong>Type:</strong> <span>xxxx</span></li>
            <li><strong>Details:</strong> <span>xxxx</span></li>
            <li><strong>Amount:</strong> <span>xxxx</span></li>
            <li class="parent"><strong>Notes</strong> <div class="note">Repeated element</div>
            <ul>
              <li><strong>Note  Title:</strong> <span>xxxx</span></li>
              <li><strong>Note Date:</strong> <span>xxxx</span></li>
              <li><strong>Note  Details:</strong> <span>xxxx</span></li>
            </ul></li>
          </ul>
			<div class="clearfix"></div>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
  </section>
  <section>
    <h1 id="section05">Appointment</h1>
    <div class="child">
    <h2>Estate Trustee </h2>
    <div class="child">
    <h3>Primary</h3>
    <div class="fieldset table">
      <ul>
        <div class="row">
          <div class="col-xs-6">
            <li class="outstand"><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
            <li class="parent"><strong>Type: </strong> <span>xxxxx</span>
              <ul>
                <li><strong>Details:</strong> <span>xxxx</span></li>
              </ul>
            </li>
          </div>
          <div class="col-xs-6">
            <li class="parent"><strong>Members: (repeated element)</strong>
              <ul>
                <li><strong>Name:</strong> <span>xxxx</span></li>
                <li><strong>Relationship:</strong> <span>xxxx</span></li>
                <li><strong>Comments:</strong> <span>xxxx</span> (if entered)</li>
              </ul>
            </li>
          </div>
        </div>
      </ul>
    </div>
		</div>
   <div class="child">
    <h3>1st Alternative </h3>
    <div class="fieldset full table">
      <ul>
        <li><strong>Outstanding </strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
        <li class="parent"><strong>Acts if:  all Primary Attorney(s) cannot act</strong> <div class="note">Displayed if selected</div>
          <ul>
            <li class="title parent"><strong>Members</strong>
              <ul class="hoz">
				  <li class="parent"><strong>Member 1</strong>
               <ul>
                <li><strong>Name:</strong> <span>xxxx</span></li>
                <li><strong>Relationship:</strong> <span>xxxx</span></li>
                <li><strong>Comments:</strong> <span>xxxx</span></li>
              </ul>
				  </li>
				  <li class="parent"><strong>Member 2</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
				<li class="parent"><strong>Member 3</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
			    <li class="parent"><strong>Member 4</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
			    <li class="parent"><strong>Member 5</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
			  </ul>
           <div class="clearfix"></div>
            </li>
            <div class="clearfix"></div>
			  <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
            <li class="parent"><strong>Type:</strong> <span>xxxxx</span>
              <ul>
                <li><strong>Details:</strong> <span>xxxx</span></li>
              </ul>
            </li>
          </ul>
			
        </li>
        <li><strong>Acts if : any 1 Primary Attorney cannot act</strong> <div class="note">Displayed if selected</div>
          <ul>
            <li class="title parent"><strong>Members</strong>
              <ul class="hoz">
				  <li class="parent"><strong>Member 1</strong>
               <ul>
                <li><strong>Name:</strong> <span>xxxx</span></li>
                <li><strong>Relationship:</strong> <span>xxxx</span></li>
                <li><strong>Comments:</strong> <span>xxxx</span></li>
              </ul>
				  </li>
				  <li class="parent"><strong>Member 2</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
				<li class="parent"><strong>Member 3</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
			  </ul>
           <div class="clearfix"></div>
            </li>
            <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
            <li class="parent"><strong>Type:</strong> <span>xxxxx</span>
              <ul>
                <li><strong>Details:</strong> <span>xxxx</span></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><strong>Acts if : {trustee name} cannot act</strong> <div class="note">Displayed if selected</div></li>
		  <li><strong>Other: </strong> <span>xxxx</span> <div class="note">Displayed if selected</div></li>
      </ul>
		<div class="clearfix"></div>
    </div>
		</div>
   <div class="child">
    <h3>2nd Alternative</h3>
    <div class="fieldset table">
      <ul>
        <li>Same as 1st Alternative</li>
      </ul>
		<div class="clearfix"></div>
    </div>
		</div>
		<div class="clearfix"></div>
		</div>
   <div class="child">
    <h2>Guardians</h2>
    <div class="fieldset table">
      <ul>
        <li><strong>Primary:</strong>  <span>xxxx</span></li>
        <li><strong>Alternative:</strong>  <span>xxxx</span></li>
        <li><strong>Other:</strong>  <span>xxxx</span></li>
      </ul>
    </div>
	   <div class="clearfix"></div>
		</div>
   <div class="child">
    <h2>Power of Attorney</h2>
    <div class="child">
    <h3>Financial </h3>
    <div class="child">
    <h4>Primary </h4>
    <div class="fieldset full table">
      <ul>
            <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
            <li class="parent"><strong>Members</strong>
              <ul class="hoz">
				  <li class="parent"><strong>Member 1</strong>
               <ul>
                <li><strong>Name:</strong> <span>xxxx</span></li>
                <li><strong>Relationship:</strong> <span>xxxx</span></li>
                <li><strong>Comments:</strong> <span>xxxx</span></li>
              </ul>
				  </li>
				  <li class="parent"><strong>Member 2</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
				<li class="parent"><strong>Member 3</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
			  </ul>		
           <div class="clearfix"></div>		
            </li>
            <div class="clearfix"></div>
             <li class="parent"><strong>Type: </strong> <span>xxxxx</span>
              <ul>
                <li><strong>Details:</strong> <span>xxxx</span></li>
              </ul>
            </li>
            
      </ul>
		<div class="clearfix"></div>
    </div>
		</div>
   <div class="child">
    <h4>1st Alternates </h4>
    <div class="fieldset full table">
      <ul>
        <li><strong>Alternates  Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
        <li class="parent"><strong>Acts if:  all Primary Attorney(s) cannot act</strong>
          <ul>
            <li class="parent"><strong>Members</strong>
              <ul class="hoz">
				  <li class="parent"><strong>Member 1</strong>
               <ul>
                <li><strong>Name:</strong> <span>xxxx</span></li>
                <li><strong>Relationship:</strong> <span>xxxx</span></li>
                <li><strong>Comments:</strong> <span>xxxx</span></li>
              </ul>
				  </li>
				  <li class="parent"><strong>Member 2</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
				<li class="parent"><strong>Member 3</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
			  </ul>
				<div class="clearfix"></div>
            </li>
            <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
            <li class="parent"><strong>Type:</strong> <span>xxxxx</span> 
              <ul>
                <li><strong>Details:</strong> <span>xxxx</span></li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="parent"><strong>Acts if : any 1 Primary Attorney cannot act</strong>
          <ul>
            <li class="parent"><strong>Members</strong>
              <ul class="hoz">
				  <li class="parent"><strong>Member 1</strong>
               <ul>
                <li><strong>Name:</strong> <span>xxxx</span></li>
                <li><strong>Relationship:</strong> <span>xxxx</span></li>
                <li><strong>Comments:</strong> <span>xxxx</span></li>
              </ul>
				  </li>
				  <li class="parent"><strong>Member 2</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
				<li class="parent"><strong>Member 3</strong>
				    <ul>
				      <li><strong>Name:</strong> <span>xxxx</span></li>
				      <li><strong>Relationship:</strong> <span>xxxx</span></li>
				      <li><strong>Comments:</strong> <span>xxxx</span></li>
			        </ul>
			    </li>
			  </ul>
				<div class="clearfix"></div>
            </li>
            <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
            <li class="parent"><strong>Type:</strong> <span>xxxxx</span> 
              <ul>
                <li><strong>Details:</strong> <span>xxxx</span></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><strong>Acts if : {trustee name} cannot act</strong></li>
        <li><strong>Other:</strong> <span>xxxxxx</span></li>
      </ul>
    </div>
		</div>
   <div class="child">
    <h4>2nd Alternates (if 1st Alternates cannot  act)</h4>
    <ul>
      <li>Same as 1st Alternative</li>
    </ul>
		</div>
	   </div>
   <div class="child">
    <h3>Personal Care </h3>
    <ul>
      <li>(same  as Financial)</li>
    </ul>
	   </div>
	   </div>
  </section>
  <section>
   
    <h1 id="section06">Surviving Spouse / No Spouse / No Descendants</h1>
    <div class="child">
    <h2> Joint Assets And  Designated Assets</h2>
    <div class="child">
    <h3>Spouse</h3>
    <div class="fieldset col2 table">
      <ul>
        <li><strong>Name:</strong> <span>xxxx</span></li>
        <li class="parent"><strong>Assigned Assets
          </strong>
          <ul>
            <li><strong>Asset 1:</strong> <span>Type,  Description</span></li>
            <li><strong>Asset 2:</strong> <span>Type,  Description</span></li>
          </ul>
        </li>
      </ul>
    </div>
		<div class="clearfix"></div>
		</div>
   <div class="child">
    <h3> Other person</h3>
    <div class="fieldset col2 table">
      <ul>
        <li><strong>Name:</strong> <span>xxxx</span></li>
        <li  class="parent"><strong>Assigned Assets
          </strong>
          <ul>
            <li><strong>Asset 1:</strong> <span>Type,  Description, Amount</span></li>
            <li><strong>Asset 2:</strong> <span>Type,  Description, Amount</span></li>
          </ul>
        </li>
      </ul>
    </div>
	   <div class="clearfix"></div>
		</div>
	  </div>
	  <div class="child">
    <h2>Initial Gifts</h2>
    <div class="child">
    <h3>Priority 1</h3>
    <div class="fieldset gifts-v2 table">
      <ul>
        <li  class="title gift"><strong>Gift #1</strong>
          <ul>
            <div class="row">
            <div class="col-xs-6">
            <li class="title parent"><strong>Beneficiary Name(s)</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Details:</strong> <span>xxxx</span></li>
            </ul></li>
            <li class="title"><strong>Gift Type:</strong> <span>Trust/Asset</span></li>
            
            <li class="title parent"><strong>Assets</strong> <div class="note">Displayed if selected</div>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Details:</strong> <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Duration</strong>
            <ul>
				<li><strong>Outstanding  </strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Details:</strong>  <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Trustee(s) </strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong>  <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Details:</strong>  <span>xxxx</span></li>
            </ul></li>
            
			<li class="title parent"><strong>Mandatory Payments</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong>  <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li class="parent"><strong>Annual Amount</strong> <div class="note">Displayed if selected</div>
              <ul>
                <li><strong>Amount:</strong>  <span>xxxx</span></li>
                <li><strong>Indexed:</strong>  <span>xxxx</span></li>
                <li><strong>Commencement:</strong> <span>xxxx</span>  </li>
                <li><strong>Termination:</strong> <span>xxxx</span></li>
                <li><strong>Suspension:</strong>  <span>xxxx</span></li>
              </ul></li>
              <li class="title parent"><strong>Staged</strong> <div class="note">Displayed if selected</div>
              <ul>
                <li><strong>Stage  Number:</strong> <span>xxxx</span></li>
                <li><strong>Age:</strong> <span>xxxx</span></li>
                <li><strong>Amount:</strong>  <span>xxxx</span></li>
                <li><strong>Stage  Number:</strong> <span>xxxx</span></li>
                <li><strong>Age:</strong> <span>xxxx</span></li>
                <li><strong>Amount:</strong>  <span>xxxx</span></li>
              </ul></li>
              <li class="parent"><strong>Other</strong> <div class="note">Displayed if selected</div>
              <ul>
                <li><strong>Details:</strong>  <span>xxxx</span></li>
              </ul></li>
			  </ul></li>
           
           <li class="title parent"><strong>Discretionary Payments</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>None</strong> <div class="note">Displayed if selected</div></li>
              <li><strong>Unlimited Discretion</strong> <div class="note">Displayed if selected</div></li>
              <li class="parent"><strong>Limited Discretion</strong> <div class="note">Displayed if selected</div>
                <ul>
                <li><strong>Only if used up own assets</strong></li>
                <li><strong>Annual amount limits:</strong> <span>xxxx</span></li>
                <li><strong>Total aggregate limit:</strong> <span>xxxx</span></li>
                <li><strong>Min. capital balance:</strong> <span>xxxx</span></li>
                <li><strong>Other:</strong> <span>xxxx</span></li>
              </ul></li>
              <li class="parent"><strong>Termination / Suspension of Payments?</strong> <div class="note">Displayed if selected</div>
              <ul>
                <li><strong>Insolvency</strong></li>
                <li><strong>Remarriage:</strong> <span>Termination/Suspension</span></li>
                <li><strong>Cohabitation:</strong> <span>Termination/Suspension</span></li>
                <li><strong>Other:</strong> <span>xxxx</span></li>
              </ul></li>
			  </ul></li>
           
           
           		</div>
            <div class="col-xs-6">
            
            
            <li class="title parent"><strong>Termination of Use</strong> <div class="note">Only display selected options</div>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Death</strong></li>
              <li><strong>Remarriage</strong></li>
              <li><strong>Cohabitation</strong></li>
              <li><strong>Failure To  Pay Expenses</strong></li>
              <li><strong>Consent Of  Beneficiary</strong></li>
              <li><strong>Trustee's  Discretion</strong></li>
              <li><strong>Other:</strong>   <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Replacement Property</strong> <div class="note">Only display selected options</div>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Beneficiary  Consent required?</strong></li>
              <li class="parent"><strong>Excess  Proceeds</strong>
<ul>
  <li><strong>Residue</strong></li>
                <li><strong>Trust For  Beneficiary</strong></li>
                <li><strong>Other:</strong> <span>xxxx</span></li>
            </ul></li>
              <li><strong>Other:</strong>   <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Expenses</strong> <div class="note">Only display selected options</div>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Paid By Estate (%):</strong> <span>xxxx</span></li>
              <li><strong>Paid By Beneficiary (%):</strong> <span>xxxx</span></li>
              <li><strong>Separate  Fund:</strong> <span>xxxx</span></li>
              <li><strong>Other:</strong> <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Option To Purchase</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>None</strong></li>
              <li class="parent"><strong>Participant(s)</strong>
              <ol>
                <li>Name </li>
                <li>Name…</li>
              </ol></li>
              <li><strong>Valuation:</strong> <span>xxxx</span></li>
              <li><strong>Timing:</strong> <span>xxxx</span></li>
              <li><strong>Condition(s):</strong> <span>xxxx</span></li>
              <li><strong>Special Provisions:</strong> <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Gift-Over at End of Trust</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong>  <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>To Residue</strong></li>
              <li><strong>Other:</strong> <span>xxxx</span></li>
            </ul></li>
            <li class="title"><strong>Priority of Payment:</strong> <span>xxxxx</span></li>
            <li class="title"><strong>Other Provisions:</strong> <span>xxxx</span></li>
            
				</div>
			  </div>
			  
			  <li class="title parent"><strong>Notes</strong> <div class="note">Repeated element</div>
            <ul>
              <li><strong>Note  Title:</strong> <span>xxxx</span></li>
              <li><strong>Note Date:</strong> <span>xxxx</span></li>
              <li><strong>Note  Details:</strong> <span>xxxx</span></li>
            </ul></li>
          </ul>
          
        </li>
		  
        <li class="title gift parent"><strong>Gift #2</strong>
          <ul>
            <div class="row">
            <div class="col-xs-6">
            <li class="title parent"><strong>Beneficiary Name(s)</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Details:</strong> <span>xxxx</span></li>
            </ul></li>
            <li class="title"><strong>Gift Type:</strong> <span>Trust/Asset</span></li>
            
            <li class="title parent"><strong>Assets</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Details:</strong> <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Duration</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Details:</strong>  <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Trustee(s) </strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong>  <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Details:</strong>  <span>xxxx</span></li>
            </ul></li>
            
			<li class="title parent"><strong>Mandatory Payments</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong>  <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li class="parent"><strong>Annual Amount</strong> <div class="note">Displayed if selected</div>
              <ul>
                <li><strong>Amount:</strong>  <span>xxxx</span></li>
                <li><strong>Indexed:</strong>  <span>xxxx</span></li>
                <li><strong>Commencement:</strong> <span>xxxx</span>  </li>
                <li><strong>Termination:</strong> <span>xxxx</span></li>
                <li><strong>Suspension:</strong>  <span>xxxx</span></li>
              </ul></li>
              <li class=" title parent"><strong>Staged</strong> <div class="note">Displayed if selected</div>
              <ul>
                <li><strong>Stage  Number:</strong> <span>xxxx</span></li>
                <li><strong>Age:</strong> <span>xxxx</span></li>
                <li><strong>Amount:</strong>  <span>xxxx</span></li>
                <li><strong>Stage  Number:</strong> <span>xxxx</span></li>
                <li><strong>Age:</strong> <span>xxxx</span></li>
                <li><strong>Amount:</strong>  <span>xxxx</span></li>
              </ul></li>
              <li class="parent"><strong>Other</strong> <div class="note">Displayed if selected</div>
              <ul>
                <li><strong>Details:</strong>  <span>xxxx</span></li>
              </ul></li>
			  </ul></li>
           
           <li class="title parent"><strong>Discretionary Payments</strong> <div class="note">Only display selected options</div>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>None</strong></li>
              <li><strong>Unlimited Discretion</strong></li>
              <li class="parent"><strong>Limited Discretion</strong>
                <ul>
                <li><strong>Only if used up own assets</strong></li>
                <li><strong>Annual amount limits: </strong> <span>xxxx</span></li>
                <li><strong>Total aggregate limit:</strong> <span>xxxx</span></li>
                <li><strong>Min. capital balance:</strong> <span>xxxx</span></li>
                <li><strong>Other:</strong><strong> </strong> <span>xxxx</span></li>
              </ul></li>
              <li class="parent"><strong>Termination / Suspension of Payments?</strong> <div class="note">Only display selected options</div>
              <ul>
                <li><strong>Insolvency</strong></li>
                <li><strong>Remarriage:</strong> <span>Termination/Suspension</span></li>
                <li><strong>Cohabitation:</strong> <span>Termination/Suspension</span></li>
                <li><strong>Other:</strong> <span>xxxx</span></li>
              </ul></li>
			  </ul></li>
           
           
           		</div>
            <div class="col-xs-6">
            
            
            <li class="title parent"><strong>Termination of Use</strong> <div class="note">Only display selected options</div>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Death</strong></li>
              <li><strong>Remarriage</strong></li>
              <li><strong>Cohabitation</strong></li>
              <li><strong>Failure To  Pay Expenses</strong></li>
              <li><strong>Consent Of  Beneficiary</strong></li>
              <li><strong>Trustee's  Discretion</strong></li>
              <li><strong>Other:</strong>   <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Replacement Property</strong> <div class="note">Only display selected options</div>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Beneficiary  Consent required?</strong></li>
              <li class="parent"><strong>Excess  Proceeds</strong>
              <ul>
                <li><strong>Residue</strong></li>
                <li><strong>Trust For Beneficiary</strong></li>
                <li><strong>Other:</strong> <span>xxxx</span></li>
            </ul></li>
              <li><strong>Other:</strong>   <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Expenses</strong> <div class="note">Only display selected options</div>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Paid By Estate (%):</strong> <span>xxxx</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Paid By Beneficiary (%):</strong> <span>xxxx</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Separate Fund:</strong> <span>xxxx</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Other:</strong> <span>xxxx</span> <div class="note">Displayed if selected</div></li>
            </ul></li>
            <li class="title parent"><strong>Option To Purchase</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>None</strong> <div class="note">Displayed if selected</div></li>
              <li  class="parent"><strong>Participant(s)</strong> <div class="note">Displayed if selected</div>
              <ol>
                <li>Name </li>
                <li>Name…</li>
              </ol></li>
              <li><strong>Valuation:</strong> <span>xxxx</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Timing:</strong> <span>xxxx</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Condition(s):</strong> <span>xxxx</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Special Provisions:</strong> <span>xxxx</span> <div class="note">Displayed if selected</div></li>
            </ul></li>
            <li class="title parent"><strong>Gift-Over at End of Trust</strong> <div class="note">Only display selected options</div>
            <ul>
              <li><strong class="outstanding">Outstanding</strong>  <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>To Residue</strong></li>
              <li><strong>Other:</strong> <span>xxxx</span></li>
            </ul></li>
            <li class="title"><strong>Priority of  Payment:</strong> <span>xxxxx</span></li>
            <li class="title"><strong>Other  Provisions:</strong> <span>xxxx</span></li>
            
				</div>
			  </div>
			  <li class="title parent"><strong>Notes</strong> <div class="note">Repeated element</div>
            <ul>
              <li><strong>Note  Title:</strong> <span>xxxx</span></li>
              <li><strong>Note Date:</strong> <span>xxxx</span></li>
              <li><strong>Note  Details:</strong> <span>xxxx</span></li>
            </ul></li>
          </ul>
          
        </li>
      </ul>
    </div>
		<div class="clearfix"></div>
		  </div>
		  <div class="child">
    <h3>Priority 2</h3>
    <p>Same as priority 1,  listing all gifts.</p>
		  </div>
	  </div>
	  <div class="child">
    <h2>Balance of the Estate</h2>
    <div class="child">
    <h3>Estate Summary</h3>
    <div class="fieldset table">
      <ul>
        <li class="parent"><strong>Remaining  Assets</strong>
          <ul>
            <li><strong>Asset 1:</strong> <span>Type,  Description</span></li>
            <li><strong>Asset 2:</strong> <span>Type,  Description</span></li>
            <li><strong>Asset 3:</strong> <span>Type,  Description</span></li>
            <li><strong>Asset 4:</strong> <span>Type,  Description</span></li>
          </ul>
        </li>
        <li><strong>Gross  Total:</strong> <span>xxxx</span></li>
        <li><strong>Total  Debts:</strong> <span>xxxx</span></li>
        <li><strong>Probate  Tax:</strong> <span>xxxx</span></li>
        <li><strong>Income  Tax:</strong> <span>xxxx</span></li>
        <li><strong>Total Cash  Legacy:</strong> <span>xxxx</span></li>
        <li><strong>Net Total:</strong> <span>xxxx</span></li>
      </ul>
    </div>
		<div class="clearfix"></div>
		  </div>
		  <div class="child">
    <h3>Estate Distribution</h3>
    <div class="child">
    <h4>Segment A</h4>
    <div class="fieldset col2 table">
      <ul>
        <li><strong>Segment  A: </strong> <span>Identifier</span></li>
        <li><strong>Segment %:</strong> <span>Details</span></li>
		</ul>
	  </div>
		<div class="clearfix"></div>
       <div class="fieldset gifts-v3 table">
		   <ul><li class="parent"><h4>Gifts</h4> 
        <ul>
          <li  class="title gift parent"><strong>Gift #1</strong>
          <ul>
            <div class="row">
            <div class="col-xs-6">
             <li class="title parent"><strong>Beneficiary Name(s)</strong>
              <ul>
                <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                <li><strong>Details:</strong> <span>xxxx</span></li>
              </ul>
            </li>
            <li class="title"> <strong> Gift Type:</strong> <span>Trust/Cash</span></li>
            
            <li class="title parent"><strong>Amount</strong>
              <ul>
                <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                <li><strong>Amount:</strong> <span>xxxx</span></li>
                <li><strong>Currency:</strong> <span>xxxx</span></li>
                <li><strong>Indexing:</strong> <span class="hidden">True/False</span></li>
                <li><strong>Specific  Source:</strong> <span>xxxx</span></li>
              </ul>
            </li>
            <li class="title parent"><strong>Duration</strong>
              <ul>
                <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                <li><strong>Beneficiary&rsquo;s  Lifetime:</strong> <span>xxxx</span></li>
                <li><strong>Beneficiary  Attaining Age:</strong> <span>xxxx</span></li>
                <li><strong>Other:</strong> <span>xxxx</span></li>
                <li><strong>Earliest  Of / Latest Of</strong></li>
              </ul>
            </li>
            <li class="title parent"><strong>Trustee(s) </strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong><span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Same as  Executors / Other:</strong> <span>xxxx</span></li>
              <li><strong>Alternate:</strong> <span>xxxx</span></li>
            </ul></li>
			
          <li class="title parent"><strong>Gift-Over at End of Trust</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>To Residue</strong></li>
              <li><strong>Other:</strong> <span>xxxx</span> 
              </li>
            </ul></li>
            <li class="title"><strong>Priority of  Payment:</strong> <span>xxxx</span></li>
            <li class="title"><strong>Other  Provisions:</strong> <span>xxxx</span></li>         	
          	
          			</div>
           <div class="col-xs-6">
            <li class="title parent"><strong>Mandatory Payments</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong><span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>None</strong></li>
              <li class="title parent"><strong>Staged</strong>
              <ul>
                <li><strong>Stage Number:</strong> <span>xxxx</span></li>
                <li><strong>Age:</strong> <span>xxxx</span></li>
                <li><strong>Amount:</strong> <span>xxxx</span></li>
                <li><strong>Stage Number:</strong> <span>xxxx</span></li>
                <li><strong>Age:</strong> <span>xxxx</span></li>
                <li><strong>Amount:</strong> <span>xxxx</span></li>
              </ul></li>
              <li><strong>Income:</strong> <span>xxxx</span></li>
              <li class="parent"><strong>Annual Amount</strong>
              <ul>
                <li><strong>Annual Amount:</strong> <span>xxxx</span></li>
                <li><strong>Indexed:</strong> <span class="hidden">True/False</span></li>
                <li><strong>Commencement:</strong> <span>xxxx</span></li>
                <li><strong>Termination:</strong> <span>xxxx</span></li>
                <li><strong>Suspension:</strong> <span>xxxx</span></li>
              </ul></li>
              <li><strong>Other:</strong> <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Discretionary Payments</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>None</strong></li>
              <li><strong>Unlimited Discretion</strong></li>
              <li class="parent"><strong>Limited Discretion</strong> 
                <ul>
                  <li><strong>Only if used up own assets</strong></li>
                  <li><strong>Annual amount limits:</strong> <span>xxxx</span></li>
                  <li><strong>Total aggregate limit:</strong> <span>xxxx</span></li>
                  <li><strong>Min. capital balance: </strong> <span>xxxx</span></li>
                  <li><strong>Other:</strong> <span>xxxx</span> 
                  </li>
                </ul>
              </li>
              <li class="title parent"><strong>Termination / Suspension of  Payments?</strong>
              <ul>
                <li><strong>Insolvency</strong></li>
                <li><strong>Remarriage:</strong> <span>Termination/ Suspension</span></li>
                <li><strong>Cohabitation:</strong> <span>Termination/ Suspension</span></li>
                <li><strong>Other:</strong> <span>xxxx</span></li>
              </ul></li>
            </ul></li>
            
				</div>
			  </div>
         <li class="title parent"><strong>Notes</strong> <div class="note">Repeated element</div>
              <ul>
                <li><strong>Note  Title:</strong> <span>xxxx</span></li>
                <li><strong>Note Date:</strong> <span>xxxx</span></li>
                <li><strong>Note  Details:</strong> <span>xxxx</span></li>
              </ul>
            </li>
          </ul>
        </li>
        <li  class="title gift parent"><strong>Gift #2</strong>
          <ul>
            <div class="row">
            <div class="col-xs-6">
             <li class="title parent"><strong>Beneficiary Name(s)</strong>
              <ul>
                <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                <li><strong>Details:</strong> <span>xxxx</span></li>
              </ul>
            </li>
            <li class="title"> <strong> Gift Type:</strong> <span>Trust/Cash</span></li>
            
            <li class="title parent"><strong>Amount</strong>
              <ul>
                <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                <li><strong>Amount:</strong> <span>xxxx</span></li>
                <li><strong>Currency:</strong> <span>xxxx</span></li>
                <li><strong>Indexing:</strong> <span class="hidden">True/False</span></li>
              </ul>
            </li>
            <li class="title parent"><strong>Duration</strong>
              <ul>
                <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                <li><strong>Beneficiary  Attaining Age:</strong> <span>xxxx</span> 
                </li>
                <li><strong>Earliest  Of / Latest Of</strong></li>
              </ul>
            </li>
            <li class="title parent"><strong>Trustee(s) </strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Same as  Executors / Other:</strong> <span>xxxx</span></li>
            </ul></li>
			
                 	
          	
          			</div>
           <div class="col-xs-6">
            
            <li class="title parent"><strong>Mandatory Payments</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong><span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>None</strong></li>
            </ul></li>
            <li class="title parent"><strong>Discretionary Payments</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>None</strong></li>
            </ul></li>
            <li class="title parent"><strong>Gift-Over at End of Trust</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>To Residue</strong></li>
            </ul></li>
            <li class="title"><strong>Priority of  Payment:</strong> <span>xxxx</span></li>
            <li class="title"><strong>Other  Provisions:</strong> <span>xxxx</span></li>  
				</div>
			  </div>
         <li class="title parent"><strong>Notes</strong> <div class="note">Repeated element</div>
              <ul>
                <li><strong>None</strong></li>
              </ul>
            </li>
          </ul>
        </li>
        <div class="clearfix"></div>
    </ul>
        </li>
        <div class="clearfix"></div>
      </ul>
		<div class="clearfix"></div>
		  </div>
			  <div class="clearfix"></div>
			  </div>
			  
			  
		<div class="child">
    <h4>Segment B</h4>
    <div class="fieldset col2 table">
      <ul>
        <li><strong>Segment  B: </strong> <span>Identifier</span></li>
        <li><strong>Segment %:</strong> <span>Details</span></li>
		</ul>
	  </div>
		<div class="clearfix"></div>
       <div class="fieldset gifts-v3 table">
       <ul><li class="parent"><h4>Gifts</h4> 
        <ul>
        <li  class="title gift parent"><strong>Gift #1</strong>
          <ul>
            <div class="row">
            <div class="col-xs-6">
             <li class="title parent"><strong>Beneficiary Name(s)</strong>
              <ul>
                <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                <li><strong>Details:</strong> <span>xxxx</span></li>
              </ul>
            </li>
            <li class="title"> <strong> Gift Type:</strong> <span>Trust/Cash</span></li>
            
            <li class="title parent"><strong>Amount</strong>
              <ul>
                <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                <li><strong>Amount:</strong> <span>xxxx</span></li>
                <li><strong>Currency:</strong> <span>xxxx</span></li>
                <li><strong>Indexing:</strong> <span class="hidden">True/False</span></li>
                <li><strong>Specific  Source:</strong> <span>xxxx</span></li>
              </ul>
            </li>
            <li class="title parent"><strong>Duration</strong>
              <ul>
                <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                <li><strong>Beneficiary&rsquo;s  Lifetime:</strong> <span>xxxx</span></li>
				<li><strong>Beneficiary  Attaining Age:</strong> <span>xxxx</span></li>
                <li><strong>Other:</strong> <span>xxxx</span></li>
                <li><strong>Earliest  Of / Latest Of</strong></li>
              </ul>
            </li>
            <li class="title parent"><strong>Trustee(s) </strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>Same as  Executors / Other:</strong> <span>xxxx</span></li>
              <li><strong>Alternate:</strong> <span>xxxx</span></li>
            </ul></li>			
          <li class="title parent"><strong>Gift-Over at End of Trust</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>To Residue</strong></li>
              <li><strong>Other:</strong> <span>xxxx</span> 
              </li>
            </ul></li>
            <li class="title"><strong>Priority of  Payment:</strong> <span>xxxx</span></li>
            <li class="title"><strong>Other  Provisions:</strong> <span>xxxx</span></li>         	
          	
          			</div>
           <div class="col-xs-6">
            <li class="title parent"><strong>Mandatory Payments</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>None</strong></li>
              <li class="title parent"><strong>Staged</strong>
              <ul>
                <li><strong>Stage Number:</strong> <span>xxxx</span></li>
                <li><strong>Age:</strong> <span>xxxx</span></li>
                <li><strong>Amount:</strong> <span>xxxx</span></li>
                <li><strong>Stage Number:</strong> <span>xxxx</span></li>
                <li><strong>Age:</strong> <span>xxxx</span></li>
                <li><strong>Amount:</strong> <span>xxxx</span></li>
              </ul></li>
              <li><strong>Income:</strong> <span>xxxx</span></li>
              <li class="parent"><strong>Annual Amount</strong>
              <ul>
                <li><strong>Annual Amount:</strong> <span>xxxx</span></li>
                <li><strong>Indexed:</strong> <span class="hidden">True/False</span></li>
                <li><strong>Commencement:</strong> <span>xxxx</span></li>
                <li><strong>Termination:</strong> <span>xxxx</span></li>
                <li><strong>Suspension:</strong> <span>xxxx</span></li>
              </ul></li>
              <li><strong>Other:</strong> <span>xxxx</span></li>
            </ul></li>
            <li class="title parent"><strong>Discretionary Payments</strong>
            <ul>
              <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
              <li><strong>None</strong></li>
              <li><strong>Unlimited Discretion</strong></li>
              <li class="parent"><strong>Limited Discretion</strong>
                <ul>
                  <li><strong>Only if used up own assets</strong></li>
                  <li><strong>Annual amount limits:</strong> <span>xxxx</span></li>
                  <li><strong>Total aggregate limit:</strong> <span>xxxx</span></li>
                  <li><strong>Min. capital balance:</strong> <span>xxxx</span></li>
                  <li><strong>Other:</strong> <span>xxxx</span> 
                  </li>
                </ul>
              </li>
              <li class="parent"><strong>Termination / Suspension of  Payments?</strong>
              <ul>
                <li><strong>Insolvency</strong></li>
                <li><strong>Remarriage:</strong> <span>Termination/ Suspension</span></li>
                <li><strong>Cohabitation:</strong> <span>Termination/ Suspension</span></li>
                <li><strong>Other</strong></li>
                <ul>
                  <li><strong>Details:</strong> <span>xxxx</span></li>
                </ul>
              </ul></li>
            </ul></li>
            
				</div>
			  </div>
         <li class="title parent"><strong>Notes</strong> <div class="note">Repeated element</div>
              <ul>
                <li><strong>Note  Title:</strong> <span>xxxx</span></li>
                <li><strong>Note Date:</strong> <span>xxxx</span></li>
                <li><strong>Note  Details:</strong> <span>xxxx</span></li>
              </ul>
            </li>
          </ul>
        </li>
        <li  class="title gift parent"><strong>Gift #2</strong>
          <ul>
            <div class="row">
              <div class="col-xs-6">
                <li class="title parent"><strong>Beneficiary Name(s)</strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>Details:</strong> <span>xxxx</span></li>
                  </ul>
                </li>
                <li class="title"><strong>Gift Type:</strong> <span>Residue/Spouse/Trust</span></li>
                
                <li class="title parent"><strong>Percentage of Segment</strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>Size:</strong> <span>Entire Segment / % Portion of segment / Other Amound</span></li>
                    <li><strong>Details:</strong> <span>xxxx</span></li>
                    <li><strong>Condition:</strong> <span>Greater of the above/Lesser of the above</span></li>
                  </ul>
                </li>
                <li class="title parent"><strong>Trustee(s) </strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>Details:</strong> <span>xxxx</span></li>
					</ul></li>
                 <li class="title parent"><strong>Duration</strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>Details:</strong> <span>xxxx</span></li>
                  </ul>
                </li>
                <li class="title parent"><strong>Mandatory Payments</strong>
                     <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>None</strong></li>
                    <li><strong>Income</strong></li>
                    <li class="parent"><strong>Annual Amount</strong>
                      <ul>
                        <li><strong>Amount:</strong> <span>xxxxx</span></li>
                        <li><strong>Indexed:</strong> <span class="hidden">True/False</span></li>
                        <li><strong>Commencement:</strong> <span>xxxx</span></li>
                        <li><strong>Termination:</strong> <span>xxxx</span></li>
                        <li><strong>Suspension:</strong> <span>xxxxx</span></li>
                      </ul>
                    </li>
                    <li><strong>Other:</strong> <span>xxxxx</span>
                    </li>
                  </ul>
                </li>
                
              </div>
              <div class="col-xs-6">
                
                <li class="title parent"><strong>Discretionary Payments</strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>None</strong></li>
                    <li><strong>Unlimited Discretion</strong></li>
                    <li class="parent"><strong>Limited Discretion </strong>
                      <ul>
                        <li><strong>Only if used up own assets</strong></li>
                        <li><strong>Annual amount limits: </strong><span>xxxx</span></li>
                        <li><strong>Total aggregate limit:</strong> <span>xxxx</span></li>
                        <li><strong>Min. capital balance:</strong> <span>xxxx</span></li>
                        <li><strong>Other:</strong> <span>xxxx</span></li>
                        </ul>
                      </li>                    
                    </ul>
                </li>
                
                
                <li class="title parent"><strong>Gift-Over at End of Trust</strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>As-per &quot;No Spouse&quot; Distribution</strong></li>
                    <li><strong>Client's Issue:</strong> <span>Trust/Absolute</span></li>
                     <li><strong>Spouse's Issue:</strong> <span>Trust/Absolute</span></li>
                     <li><strong>Power of Appointment</strong></li>
                     <li><strong>Other:</strong> <span>xxxx</span>
                     </li>
                  </ul>
                </li>
                
                <li class="title"><strong>Other  Provisions:</strong> <span>xxxx</span></li>
              </div>
            </div>
            <li class="title parent"><strong>Notes</strong> <div class="note">Repeated element</div>
                  <ul>
                    <li><strong>Note  Title:</strong> <span>xxxxx</span></li>
                    <li><strong>Note Date:</strong> <span>xxxxx</span></li>
                    <li><strong>Note  Details:</strong> <span>xxxxx</span></li>
                  </ul>
                </li>
          </ul>
        </li>
        <li  class="title gift parent"><strong>Gift #3</strong>
          <ul>
            <div class="row">
              <div class="col-xs-6">
                <li class="title parent"><strong>Beneficiary Name(s)</strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>Details:</strong> <span>xxxx</span></li>
                  </ul>
                </li>
                <li class="title"><strong>Gift Type:</strong> <span>Residue/Spouse/Trust</span></li>
                
                <li class="title parent"><strong>Percentage of Segment</strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>Size:</strong> <span>Entire Segment</span></li>
                    <li><strong>Condition:</strong> <span>Greater of the above/Lesser of the above</span></li>
                  </ul>
                </li>
                <li class="title parent"><strong>Trustee(s) </strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>Details:</strong> <span>Same as Executor (s)</span></li>
					</ul></li>
                  <li class="title parent"><strong>Duration</strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>Details:</strong> <span>Spouse's Lifetime</span></li>
                  </ul>
                </li>
                
                
              </div>
              <div class="col-xs-6">
                <li class="title parent"><strong>Mandatory Payments</strong>
                     <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>None</strong></li>
                  </ul>
                </li>
                <li class="title parent"><strong>Discretionary Payments</strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>Unlimited Discretion</strong></li>                    
                    </ul>
                </li>
                
                
                <li class="title parent"><strong>Gift-Over at End of Trust</strong>
                  <ul>
                    <li><strong class="outstanding">Outstanding</strong> <span class="hidden">True/False</span> <div class="note">Displayed if selected</div></li>
                    <li><strong>As-per &quot;No Spouse&quot; Distribution</strong></li>
                  </ul>
                </li>
                
                <li class="title"><strong>Other  Provisions:</strong> <span>xxxx</span>
                </li>
              </div>
            </div>
            <li class="title parent"><strong>Notes</strong> <div class="note">Repeated element</div>
                  <ul>
                    <li><strong>None</strong></li>
                  </ul>
                </li>
          </ul>
        </li>
        
        <div class="clearfix"></div>
      </ul>
        </li>
        <div class="clearfix"></div>
      </ul>
		<div class="clearfix"></div>
		  </div>
			  <div class="clearfix"></div>
			  </div>
    </div>
	  </div>
  </section>
</div>
</body>


</html>
