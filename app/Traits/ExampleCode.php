<?php namespace App\Traits;

trait ExampleCode
{
    public function printThis()
    {
        echo "Trait executed";
       exit;
    }

    public function anotherMethod()
    {
        echo "Trait – anotherMethod() executed";
    }
}