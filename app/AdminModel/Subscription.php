<?php

namespace App\AdminModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    use SoftDeletes;
    /**
     *The attributes that are mass assignable.
     * 
     * @var type 
     */
    protected $dates = ['deleted_at'];
    protected $table = 're_subscription_type';
    protected $fillable = [
        'type','cost','no_of_songs',
    ];
}
