<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Datatables;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AdminModel\Video as Video;

class ChartController extends Controller {

    function index() {

        $video = Video::select(\DB::raw("count(id) as count,artist"))
                        ->orderBy("created_at")
                        ->groupBy(\DB::raw("artist"))
                        ->get()->toArray();

        $response = array();
        foreach ($video as $result) {
            $response[$result['artist']] = (int) $result['count'];
        }
        return view('admin.charts.index', ['video' => $response]);
    }

}
